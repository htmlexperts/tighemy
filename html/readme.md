How to start project:

1. Install packages `npm i`.
2. Run `gulp` for serve.
3. Run `gulp build` for make build with minify.

Path's: 

1. HTML content in json format `source/handlebars/*component*/data/*.js` files.
2. HTML structure of components in `/source/handlebars/*component*/*.hbs`.
3. HTML pages main html and includes components in `source/pages/*.html`.
4. SCSS in `source/stylesheets` folder, all files imported `base.scss`.
5. JS in `source/js` folder.
6. Content images in `source/images/content` folder.
7. General images in `source/images/general` folder.
8. Other files and folders pickup on `source/misc` folder and his migrate in main folder.