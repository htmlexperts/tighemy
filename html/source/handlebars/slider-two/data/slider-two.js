twoslider:{
    slide:[
        {
            src: "source/images/content/homeware/3.jpg",
            text: "Bowls, $25-$30; Plates, $150 for set of 3; Tumbler, $25; Pitcher, $45; Condiment Bowl, $45",
            class: "big-slide"   
        },
        {
            src: "source/images/content/homeware/5.jpg",
            text: ""   
        },
        {
            src: "source/images/content/homeware/4.jpg",
            text: "Rustic Berber Cushions, $110-$160"   
        },
        {
            src: "source/images/content/homeware/1.jpg",
            text: "Salad Bowl, $150"   
        },
        {
            src: "source/images/content/homeware/2.jpg",
            text: "Tea Glasses Small, $9 and Large, $12"   
        }
    ]
},
luggage:{
    slide:[
        {
            src: "source/images/content/luggage/1.jpg",
            text: "Leather Weekend Bag, $650 (also available in Pony, $1,100)"   
        },
        {
            src: "source/images/content/luggage/2.jpg",
            text: "Velvet Tunic Coat in Black, $850; Black Pony Handbag, $625"   
        },
        {
            src: "source/images/content/luggage/3.jpg",
            text: ""   
        },
        {
            src: "source/images/content/luggage/4.jpg",
            text: "Leather Tote Bag Small, $450 and Large, $475 (available in tobacco, slate blue, grey and ivory)"   
        },
        {
            src: "source/images/content/luggage/5.jpg",
            text: "Handbag in Black Pony, $625 (also available in rich red, dark green and white)"   
        },
        {
            src: "source/images/content/luggage/6.jpg",
            text: ""   
        }
    ]
}