innerpage:{
    entertain:{
        rowone:{
            title: "entertain at home",
            text: "A unique collection of tableware,<br/> glassware and soft furnishings authentically created and handmade by the finest<br/> artisans working in Morocco today.",
            textimg: "source/images/content/inner-page/entertain/entertain1.jpg",
            price: 'Cushions from $110',
            position: "top-left white",
            imgone:{
                src: "source/images/content/inner-page/entertain/entertain2.jpg",
                price: "Salad Bowl, $150",
                position: " top-right white"
            },
            imgtwo:{
                src: "source/images/content/inner-page/entertain/entertain3.jpg"
            }
        },
        rowtwo:{
            title: "“MOROCCANS ARE FAMOUS FOR THEIR HOSPITALITY AND SERVING GREEN TEA WITH MINT SETS<br/> THE TONE FOR A SOPHISTICATED EVENING WITH FRIENDS.”",
            textimg: "source/images/content/inner-page/entertain/entertain4.jpg",
            texttrans: '-24',
            textcenter: 'entertain',
            imgone:{
                src: "source/images/content/inner-page/entertain/entertain5.jpg"

            },
            imgtwo: {
                src: "source/images/content/inner-page/entertain/entertain6.jpg",
                trnas: '-64',
                price: "Tea Glasses from $9",
                position: "top-right black"
            }
        },
        nav:[
            {
                link: "Entertain",
                class: "active ajax-link",
                href: "ajax/entertain.html"
            },
            {
                link: "escape",
                class: "ajax-link",
                href: "ajax/escape.html"
            },
            {
                link: "everyday",
                class: "ajax-link",
                href: "ajax/everyday.html"
            }

        ]
    },
    escape:{
        rowone:{
            title: "ESCAPE TO PARADISE",
            text: "An exquisitely crafted and designed collection of apparel, accessories and luggage for those seeking the allure<br/> of the exotic.",
            textimg: "source/images/content/inner-page/escape/escape1.jpg",
            position: " top-left white",
            price: "Classic Weekend Bag, $695",
            imgone:{
                src: "source/images/content/inner-page/escape/escape2.jpg",

                price: "Silk Kaftan, $750",
                position: "bottom-left white"
            },
            imgtwo:{
                src: "source/images/content/inner-page/escape/escape3.jpg",
                price: "Embroidered Babouches, $65",
                position: "top-right black"
            }
        },
        rowtwo:{
            title: "“FROM LUXURIOUS KAFTANS TO WEEKEND BAGS, EACH PIECE PROMISES AN ATTENTION TO<br/> DETAIL AND FINISHING THAT ARE SECOND TO NONE.”",
            textimg: "source/images/content/inner-page/escape/escape4.jpg",
            texttrans: '-24',
            textcenter: 'escape',
            imgone:{
                src: "source/images/content/inner-page/escape/escape5.jpg"
            },
            imgtwo: {
                src: "source/images/content/inner-page/escape/escape6.jpg",
                trnas: '-64',
                price: "Silk Kaftan, $750",
                position: "top-left white"
            }
        },
        nav:[
            {
                link: "Entertain",
                class: "ajax-link",
                href: "ajax/entertain.html"
            },
            {
                link: "escape",
                class: "active ajax-link",
                href: "ajax/escape.html"
            },
            {
                link: "everyday",
                class: "ajax-link",
                href: "ajax/everyday.html"
            }

        ]
    },
    everyday:{
        rowone:{
            title: "EVERYDAY LIFE",
            text: "Beautifully embroidered coats, stylish<br/> tunic tops and timeless accessories<br/> comprise Tighemi’s ‘everyday’ collection that is anything but everyday.",
            textimg: "source/images/content/inner-page/everyday/everyday1.jpg",
            position: "top-left black",
            price: " Taupe Leather Tunic Top, $575",
            imgone:{
                src: "source/images/content/inner-page/everyday/everyday2.jpg",
                price: "Bordeaux Embroidered Velvet Tunic Coat with Ivory Silk Lining, $850",
                position: "top-left white"
            },
            imgtwo:{
                src: "source/images/content/inner-page/everyday/everyday3.jpg",
                price: "Black Pony Handbag, $875",
                position: "top-left black"

            }
        },
        rowtwo:{
            title: "“DESIGNED FOR THE WOMAN<br/> WHO LIKES TO TAKE A LITTLE<br/> PIECE OF THE EXOTIC<br/> WHEREVER SHE GOES.”",
            textimg: "source/images/content/inner-page/everyday/everyday4.jpg",
            texttrans: '-24',
            textcenter: 'everyday',
            imgone:{
                src: "source/images/content/inner-page/everyday/everyday5.jpg",
                price: "Tote Bag, $575",
                position: "top-left white"
            },
            imgtwo: {
                src: "source/images/content/inner-page/everyday/everyday6.jpg",
                trnas: '-64',
                price: "Ivory Moroccan Cashmere Tunic Coat, $750 Ivory Moroccan Cashmere Cape, $550",
                position: "top-left white"
            }
        },
        nav:[
            {
                link: "Entertain",
                class: "ajax-link",
                href: "ajax/entertain.html"
            },
            {
                link: "escape",
                class: "ajax-link",
                href: "ajax/escape.html"
            },
            {
                link: "everyday",
                class: "active ajax-link",
                href: "ajax/everyday.html"
            }

        ]
    }
}