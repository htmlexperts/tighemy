oneslider:{
    slide:[
        {
            srcslide: "source/images/content/fashion/2.jpg",
            text: "Moroccan Cashmere Tunic Coat in Ivory, $750; Cape in Ivory, $550 (both available in black)",  
            class: "" 
        },
        {
            srcslide: "source/images/content/fashion/3.jpg",
            text: "Silk Kaftan and Scarf in Black, $750 (also available in midnight blue, burnt orange, ivory and print)",
            class: ""

        },
        {
            srcslide: "source/images/content/fashion/1.jpg",
            text: "Silk Kaftan and Scarf in Black, $750 (also available in midnight blue, burnt orange, ivory and print)",  
            class: "big-slide"   
        },
        {
            srcslide: "source/images/content/fashion/9.jpg",
            text: "",
            class: ""
        },
        {
            srcslide: "source/images/content/fashion/4.jpg",
            text: "Silk Kaftan and Scarf in Print, $750 (also available in midnight blue, burnt orange, ivory and black)",
            class: ""   
        },
        {
            srcslide: "source/images/content/fashion/5.jpg",
            text: "Embroidered Velvet Tunic Coat in Bordeaux, $850 (also available in black and turquoise)"
        },
        {
            srcslide: "source/images/content/fashion/6.jpg",
            text: "Velvet Embroidered Slippers, $65"   
        },
        {
            srcslide: "source/images/content/fashion/8.jpg",
            text: "Velvet Tunic Coat in Black, $850 (also available in aubergine and turquoise) Handbag in Black Pony, $625"
        },
        {
            srcslide: "source/images/content/fashion/7.jpg",
            text: "Lamb’s Leather Tunic Top in Taupe, $575 (also available in black and white)"
        }
    ]
}