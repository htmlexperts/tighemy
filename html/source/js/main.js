/**
 * Created by Eugene Khrapenko on 09.04.17.
 * email: wowhoman@gmail.com || ekh@inspired-interactive.com
 */

import $ from 'jquery';
import SiteInterface from './components/SiteInterface';

document.addEventListener('DOMContentLoaded', () => {
    window.debug = true;
    
    if($('.home-page').length){
        // console.log('.home-ready');
        // $('body').removeClass('loading');
        // $('body').addClass('load-preload');
        // setTimeout(function(){
        //     $('body').removeClass('load-preload').trigger('preload-animation-end');
        // }, 8000)
    }
});

window.addEventListener('load', () => {
    if(window.debug){
        console.log('window load');
        window.$ = $;
    }
    window.siteInterface = new SiteInterface();
});


