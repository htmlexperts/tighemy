/**
 * Created by Eugene Khrapenko on 09.04.17.
 * email: wowhoman@gmail.com || ekh@inspired-interactive.com
 */
import $ from 'jquery';
import HomePage from './pages/HomePage';
import AboutPage from './pages/AboutPage';
import SliderTwoPage from './pages/SliderTwoPage';
import SliderOnePage from './pages/SliderOnePage'; 
import ContactPage from './pages/ContactPage';
import EntertainPage from './pages/EntertainPage';

export default class SiteInterface{
    constructor(){

        this.PAGE_LIST = {
            HOME: 0,
            SLIDERTWO: 1,
            SLIDERONE: 2,
            ABOUT: 3,
            CONTACT: 4,
            ENTERTAIN: 5
        }

        this._init();
    }

    checkPage(){
        let pageId = $('body').data('page-id'),
        pages = this.PAGE_LIST;
        switch(pageId){
            case(pages.HOME):
                window.currentPage = new HomePage();
                break;
            case(pages.SLIDERTWO):
                window.currentPage = new SliderTwoPage();
                break;    
            case(pages.SLIDERONE):
                window.currentPage = new SliderOnePage();
                break;   
            case(pages.ABOUT):
                window.currentPage = new AboutPage();
                break;    
            case(pages.CONTACT):
                window.currentPage = new ContactPage();
                break;  
            case(pages.ENTERTAIN):
                window.currentPage = new EntertainPage();
                break;

        }
    }

    /**
     * 
     */
    _init(){
        this.checkPage();
    }
}