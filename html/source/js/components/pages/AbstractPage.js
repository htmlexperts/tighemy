/**
 * Created by Eugene Khrapenko on 09.04.17.
 * email: wowhoman@gmail.com || ekh@inspired-interactive.com
 */
import $ from 'jquery';
import verge from './../../plugins/verge.min.js';
import { TweenMax } from 'gsap';


export default class AbstractPage{
    constructor(){
        $.extend( verge );
        

        this.windowHeight = $.viewportH();

        this.loadedState = false;

        this.cssClasses = {
            'navLink': 'js-nav-link',
            'socialsLink': 'js-socials',
            'secondMenu': 'js-second-menu',
            'mobileMenu': 'js-mobile-menu-link',
            'whitePattern': 'white-pattern',
            'changePage': 'js-change-page'
        };

    }

    changePage(){
        let btn = $(`.${this.cssClasses.changePage}`);
        btn.on('click', (e)=>{
            e.preventDefault();
            let target = $(e.currentTarget),
                path = target.attr('href'),
                pat = $(`.${this.cssClasses.whitePattern}`),
                tl = new TimelineMax();
            if($.viewportW() > 767){
                tl.to(pat, 0.5, {alpha: 1, zIndex:10, onComplete: ()=>{
                    $('body').removeClass('menu-open');
                    console.log('close');
                    location.href = path;
                }});
            }  else{
                $('body').removeClass('menu-open');
                location.href = path;
            }

           
            console.log(path);
        });
    }

    secondMenuHoverListener(){
        let menu = $(`.${this.cssClasses.secondMenu}`),
            links = menu.find('.second-mennu a'),
            timeOut = null,
            tl = new TimelineMax();

        menu.on('mouseenter', (e)=>{ 
            clearTimeout(timeOut);
            $('.js-second-menu').addClass('active');
            tl.staggerTo(links, .2,{y:0, alpha: 1}, .2);
            console.log(timeOut);
            
        });    

        menu.on('mouseleave', (e)=> {
           timeOut = setTimeout((e)=>{
                $('.js-second-menu').removeClass('active');
                tl.staggerTo(links, .2,{y: -10, alpha: 0, onComplete: ()=>{
                    
                }}, .1);
            }, 1000);
        });
    }

    loadAnimation(){
        let item = $(`.${this.cssClasses.navLink}`),
            socials = $(`.${this.cssClasses.socialsLink}`),
            pat = $(`.${this.cssClasses.whitePattern}`),
            tl = new TimelineMax();
        if(!$('.home-page').length){
            tl
                .to(pat, 0.5,{alpha: 0, zIndex: 0})
                .to(item, .6, {alpha: 1, y: 0})
                .to(socials, 0.6, {alpha: 1, y: 0}, "-=0.6");
        } 
    }

    mobileMenuClick(){
        let link = $(`.${this.cssClasses.mobileMenu}`);

        link.on('click', (e)=>{
            e.preventDefault();
            
            if($('body').hasClass('menu-open')){
                $('body').removeClass('menu-open');
            } else{
                $('body').addClass('menu-open');
            }
        });
    }

    shareClick(){
        let face = $('.face');
        face.on('click', (e)=>{

            e.preventDefault();

            var fbpopup = window.open("https://www.facebook.com/sharer/sharer.php?u=http://stackoverflow.com", "pop", "width=600, height=400, scrollbars=no");
            return false;
           
            // let $this = $(e.currentTarget);
            // var strTitle = ((typeof $this.attr('title') !== 'undefined') ? $this.attr('title') : 'Social Share');
            // var strParam = 'width=' + 400 + ',height=' + 500 + ',resizable=' + 'yes';

            // window.open($this.attr('href'), strTitle, strParam);
        })
    }

    resize(){
        $(window).on('resize', ()=>{
            console.log('resize');
        });
    }


    /**
     * Initialize Page
     */
    _init(){
        this.secondMenuHoverListener();
        this.mobileMenuClick();
        this.loadAnimation();
        this.changePage();
        this.resize();
        // this.shareClick();
        if($.viewportW < 767){
            $(window).on('hashchange', (e)=> {
                $('body').removeClass('menu-open');
                console.log('back');

            });
        }
    }
}