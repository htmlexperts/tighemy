/**
 * Created by Eugene Khrapenko on 09.04.17.
 * email: wowhoman@gmail.com || ekh@inspired-interactive.com
 */

import AbstractPage from './AbstractPage';
import './../../plugins/swiper.min';


export default class AboutPage extends AbstractPage{
    constructor(){
        super();

        this.cssClasses = $.extend(true, {}, this.cssClasses, {
            'textBlock': 'js-about-block'
        });
        
        this._init();    
    }

    _loadAnimation(){
        let item = $(`.${this.cssClasses.textBlock}`),
            tl = new TimelineMax();

            tl.to(item, .8, {alpha: 1, y: 0});

    }

    

    _init(){
        super._init();
        console.log('About Page Init');
        setTimeout((e)=>{
            this._loadAnimation();
        }, 1000);
    }
}