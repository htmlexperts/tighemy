import AbstractPage from './AbstractPage';
import './../../plugins/swiper.min';
import imagesLoaded from 'imagesloaded';

export default class SliderOnePage extends AbstractPage{
    constructor(){
        super();

        imagesLoaded.makeJQueryPlugin( $ );
        this.cssClasses = $.extend(true, {}, this.cssClasses, {
            'slider': 'js-one-slider'
        });

        this.slider = null;
        this.windowH = $(window).height();
        this.windowW = $(window).width();
        this.loadSlider = false;
        this._init();
        
    }


    _sliderInit(){
        this.slider = new Swiper($(`.${this.cssClasses.slider}`), {
            slidesPerView: 'auto',
            nextButton: '.btn-slider-next',
            prevButton: '.btn-slider-prev',
            loop: true,
            speed: 0,
            spaceBetween: 0,
            breakpoints: {
                // when window width is <= 640px
                1023: {
                    initialSlide:0,
                    slidesPerView: 'auto',
                    speed: 600
                },
                757:{
                    spaceBetween: 10,
                    speed: 600
                }
            },

            onSlideNextStart:(swiper)=>{
                
                let textblock = $('.slide-text p'),
                    prevPrevSlide = $('.swiper-slide-prev').prev(),
                    prevSlide =  $('.swiper-slide-prev'),
                    activeSlide = $('.swiper-slide-active'),
                    nextSlide = $('.swiper-slide-next'),
                    nextNextSlide = nextSlide.next(),
                    slideWidth = $('.swiper-slide-active').width(),
                    slideText = $('.swiper-slide-active').data('text'),
                    slideTwotext = $('.swiper-slide-next').data('text'),
                    tl = new TimelineMax();


                if($.viewportW() > 1024){
                    tl.set($('.swiper-slide'), {x: 0});
                    console.log('fuck')
                    if(this.loadSlider == true){
                        
                        if(nextSlide.hasClass('big-slide')){
                            // console.log('big')
                            swiper.slideNext(true, 0);
                            tl
                                .set(prevSlide, { x: slideWidth})
                                .set(activeSlide, { x: slideWidth})
                                .set(nextSlide, { x: slideWidth, onComplete:()=>{
                                    swiper.lockSwipes();
                                }})
                                .to(textblock, 0.3,{alpha: 0, onComplete: ()=>{
                                    textblock.empty();
                                }})
                                .to(prevPrevSlide, 0.6, {x: 0})
                                .to(prevSlide, 0.6, {x: 0})
                                .to(activeSlide, 0.6, {x: 0})
                                

                        } else if(activeSlide.hasClass('big-slide')){
                            // console.log('this is big one');
                            tl
                                .to(prevPrevSlide, 0.8, {x: -(slideWidth / 2)})
                                .to(prevSlide, 0.8, {x: -(slideWidth / 2)}, "-=0.6")
                                .to(activeSlide, 0.8, {x: -(slideWidth / 2), onComplete: ()=>{
                                    textblock.eq('0').html(slideText); 
                                }}, "-=0.6")
                                .to(textblock, 0.2,{alpha:1, onComplete:()=>{
                                    swiper.unlockSwipes();
                                }});
                        } else if(prevSlide.hasClass('big-slide')){
                            tl
                                .set(prevSlide, { x: slideWidth * 2})
                                .set(activeSlide, { x: slideWidth * 2})
                                .set(nextSlide, { x: slideWidth * 2, onComplete:()=>{
                                    swiper.lockSwipes();
                                }})
                                .to(textblock, 0.3,{alpha: 0, onComplete: ()=>{
                                    textblock.empty();
                                }})
                                .to(prevSlide, 0.8, {x: 0})
                                .to(activeSlide, 0.8, {x: 0}, "-=0.6")
                                .to(nextSlide, .8, {x: 0, onComplete: ()=>{
                                    if(slideTwotext.length == 0 || slideText.length == 0){
                                        textblock.eq('0').html(slideText).css('padding-bottom', '0px');
                                        textblock.eq('1').html(slideTwotext);
                                    } else{
                                        textblock.eq('0').html("on left: " + slideText).css('padding-bottom', '10px'); 
                                        textblock.eq('1').html("on right: " + slideTwotext);
                                    }
                                }}, "-=0.6")
                                .to(textblock, 0.2,{alpha:1, onComplete:()=>{
                                    swiper.unlockSwipes();
                                }});
                        } else {
                            tl
                                .set(prevSlide, { x: slideWidth})
                                .set(activeSlide, { x: slideWidth})
                                .set(nextSlide, { x: slideWidth, onComplete:()=>{
                                    swiper.lockSwipes();
                                }})
                                .to(textblock, 0.3,{alpha: 0, onComplete: ()=>{
                                    textblock.empty();
                                    
                                }})
                                .to(prevSlide, 0.8, {x: 0}, "-=0.3")
                                .to(activeSlide, 0.8, {x: 0}, "-=0.6")
                                
                                .to(nextSlide, .8, {x: 0, onComplete: ()=>{
                                    if(slideTwotext.length == 0 || slideText.length == 0){
                                        textblock.eq('0').html(slideText).css('padding-bottom', '0px');
                                        textblock.eq('1').html(slideTwotext);
                                    } else{
                                        textblock.eq('0').html("on left: " + slideText).css('padding-bottom', '10px'); 
                                        textblock.eq('1').html("on right: " + slideTwotext);
                                    }
                                    // swiper.unlockSwipes();
                                }}, "-=0.6")
                                .to(textblock, 0.2,{alpha:1, onComplete:()=>{
                                    swiper.unlockSwipes();
                                }});
                            
                        }
                        
                    } else {
                        tl
                            .to(textblock, 0.3,{alpha: 0, onComplete: ()=>{
                                textblock.empty();
                                
                                if(slideTwotext.length == 0 || slideText.length == 0){
                                    textblock.eq('0').html(slideText).css('padding-bottom', '0px');
                                    textblock.eq('1').html(slideTwotext);
                                } else{
                                    textblock.eq('0').html("on left: " + slideText).css('padding-bottom', '10px'); 
                                    textblock.eq('1').html("on right: " + slideTwotext);
                                }
                                
                                // textblock.eq('0').html(slideText); 
                            
                            }})
                            .to(textblock, 0.2,{alpha:1});
                        return;
                    }
                    this.loadSlider = true;
                } else{
                    tl
                            .to(textblock, 0.3,{alpha: 0, onComplete: ()=>{
                                textblock.empty();
                                textblock.eq('0').html(slideText).css('padding-bottom', '0px');
                                
                                
                                // textblock.eq('0').html(slideText); 
                            
                            }})
                            .to(textblock, 0.2,{alpha:1});
                }
                
                
            },
            onSlideNextEnd:(swiper)=>{
                this.loadSlider = true;
            },
            onSlidePrevStart:(swiper)=>{
                let textblock = $('.slide-text p'),
                    prevPrevSlide = $('.swiper-slide-prev').prev(),
                    prevSlide =  $('.swiper-slide-prev'),
                    activeSlide = $('.swiper-slide-active'),
                    nextSlide = $('.swiper-slide-next'),
                    nextNextSlide = nextSlide.next(),
                    slideWidth = $('.swiper-slide-active').width(),
                    slideText = $('.swiper-slide-active').data('text'),
                    slideTwotext = $('.swiper-slide-next').data('text'),
                    tl = new TimelineMax();
                

                if($.viewportW() > 1024){
                    if(activeSlide.hasClass('big-slide')){
                        console.log('this is big');
                        swiper.slidePrev(false, 0);
                        
                        tl
                            .set(activeSlide, { x: -slideWidth})
                            .set(nextSlide, { x: -slideWidth})
                            .set(nextNextSlide, { x: -slideWidth, onComplete:()=>{
                                swiper.lockSwipes();
                            }})
                            .to(textblock, 0.3,{alpha: 0, onComplete: ()=>{
                                textblock.empty();
                            }})
                            .to(nextNextSlide, 0.8, {x: 0})
                            .to(nextSlide, 0.8, {x: 0}, "-=0.6")
                            .to(activeSlide, 0.8, {x: 0, onComplete: ()=>{
                                if(slideTwotext.length == 0 || slideText.length == 0){
                                    textblock.eq('0').html(slideText).css('padding-bottom', '0px');
                                    textblock.eq('1').html(slideTwotext);
                                } else{
                                    textblock.eq('0').html("on left: " + slideText).css('padding-bottom', '10px'); 
                                    textblock.eq('1').html("on right: " + slideTwotext);
                                }
                                swiper.unlockSwipes();
                            }}, "-=0.6")
                            .to(textblock, 0.2,{alpha:1});

                    } else if(nextSlide.hasClass('big-slide')){
                        console.log('next slide big slide');
                        
                    } else if(nextNextSlide.hasClass('big-slide')){
                        tl
                            .set(activeSlide, { x: -(slideWidth * 2)})
                            .set(nextSlide, { x: -(slideWidth * 2)})
                            .set(nextNextSlide, { x: -(slideWidth * 2), onComplete:()=>{
                                swiper.lockSwipes();
                            }})
                            .to(textblock, 0.3,{alpha: 0, onComplete: ()=>{
                                textblock.empty();
                            }})
                            .to(nextNextSlide, 0.8, {x: 0})
                            .to(nextSlide, 0.8, {x: 0}, "-=0.6")
                            .to(activeSlide, 0.8,{x: 0, onComplete: ()=>{
                                if(slideTwotext.length == 0 || slideText.length == 0){
                                    textblock.eq('0').html(slideText).css('padding-bottom', '0px');
                                    textblock.eq('1').html(slideTwotext);
                                } else{
                                    textblock.eq('0').html("on left: " + slideText).css('padding-bottom', '10px'); 
                                    textblock.eq('1').html("on right: " + slideTwotext);
                                }
                                swiper.unlockSwipes();
                            }}, "-=0.6")
                            .to(textblock, 0.2,{alpha:1});
                    } else{
                        console.log('simle');
                        tl
                            .set(activeSlide, { x: -slideWidth})
                            .set(nextSlide, { x: -slideWidth})
                            .set(nextNextSlide, { x: -slideWidth, onComplete:()=>{
                                swiper.lockSwipes();
                            }})
                            .to(textblock, 0.3,{alpha: 0, onComplete: ()=>{
                                textblock.empty();
                            }})
                            .to(nextNextSlide, 0.8, {x: 0}, "-=0.3")
                            .to(nextSlide, 0.8, {x: 0}, "-=0.6")
                            .to(activeSlide, .8, {x: 0, onComplete: ()=>{
                                if(slideTwotext.length == 0 || slideText.length == 0){
                                    textblock.eq('0').html(slideText).css('padding-bottom', '0px');
                                    textblock.eq('1').html(slideTwotext);
                                } else{
                                    textblock.eq('0').html("on left: " + slideText).css('padding-bottom', '10px'); 
                                    textblock.eq('1').html("on right: " + slideTwotext);
                                }
                                swiper.unlockSwipes();
                            }}, "-=0.6")
                            .to(textblock, 0.2,{alpha:1});
                    }
                } else{
                    tl
                        .to(textblock, 0.3,{alpha: 0, onComplete: ()=>{
                            textblock.empty();
                            textblock.eq('0').html(slideText).css('padding-bottom', '0px');
                        }})
                        .to(textblock, 0.2,{alpha:1});
                }

                
            }

        });
       
        
    }
    resize(){
        $(window).on('resize', ()=>{
            console.log('slider-resize');
            // slider.reInit();
            if($.viewportW() > 1024){
                this.propouseChange();
                // const sliderWidth = $('.swiper-slide').width() / 2; 
                // $('.swiper-slide').css('transform', 'translateX(' + sliderWidth + ')');
                // console.log('FUCK');
            }
        });
    }
    propouseChange(){

        var docWidth, docHeight, docRatio, div = $('.one-slider');
        onresize = function(){
            docWidth = $('.one-slider').width();
            docHeight = $('.one-slider').height();
            // ширина и высота вьюпорта

            docRatio = docWidth / docHeight;
            // соотношение сторон вьюпорта

            var slide =  $('.wrapp-wrapp'),
                slideW = $('.one-slider_wrapp').width(),
                slideH = $('.one-slider_wrapp').height();

               
            fullScreenProportionalElem(slide, 2048, 1280); // элемент, ширина, высота
           
        }

        function fullScreenProportionalElem(elem, width, height){
            var ratio = width / height;
            // соотношение сторон элемента

            if (docRatio < ratio){
                // return
                elem.width(docWidth);
                elem.height(Math.round(docWidth / ratio));
                elem.css('top', Math.round(docHeight / 2 - elem.offsetHeight / 2));
                // elem.css('left', '0');
                console.log('window-height');
                // высота вьюпорта больше чем высота элемента
                // ширину элемента приравниваем к ширине вьюпорта, высоту вычисляем
                // центрируем элемент по высоте
            } else if (docRatio > ratio){
                var leftCss = Math.round(docWidth / 2 - elem.offsetWidth / 2);
                
                elem.width(Math.round(docHeight * ratio));
                elem.height(docHeight);
                elem.css('top', '0px');
                // elem.style.left = Math.round(docWidth / 2 - elem.width() / 2) + 'px';
                // ширина вьюпорта больше чем ширина элемента
                // высоту элемента приравниваем к высоте вьюпорта, ширину вычисляем
                // центрируем элемент по ширине
            }
            else
            {
                
                elem.width( docWidth);
                elem.height(docHeight);
                elem.css('top', '0');
                elem.css('left', '0');
                // соотношение сторон вьюпорта равно соотношению сторон элемента
                // приравниваем стороны элемента к сторонам вьюпорта
                // обнуляем значения top и left
            }
        }


        onresize();

    }

    _imgLazyLoad(){
        let imgWrap = $('.empty');
        
        imgWrap.each(function(){
            let img = $(this).find('img').data('src');
            $(this).find('img').attr('src', img);
            $(this).imagesLoaded().done(( instance ) => {
                $(this).find('.swiper-lazy-preloader').remove();
            });
            console.log(img);
        })
    }

    _init(){
        super._init();
        console.log('one slider init');
        if($.viewportW() > 1024){
            this.propouseChange();
        }
        this._sliderInit();
        this._imgLazyLoad();
        
        // this.resize();
    }
}

