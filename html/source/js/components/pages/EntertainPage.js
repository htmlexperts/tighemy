import AbstractPage from './AbstractPage';
import './../../plugins/swiper.min';
import imagesLoaded from 'imagesloaded';

export default class EntertainPage extends AbstractPage{
    constructor(){

        super();

        imagesLoaded.makeJQueryPlugin( $ );
        this.cssClasses = $.extend(true, {}, this.cssClasses, {
            'innerItem': 'js-inner-annimete',
            'header': 'js-header',
            'ajax' : 'ajax-link'
        });
        this.headerAnimation = false;
        this.lastScrollTop = 0;
        this._init();
    }

    _setMidleH(){
        let blockHeight = $('.second .text img').height();
        $('.second .title').height(blockHeight);
    }
    _loadAnimation(time){
        let item = $(`.${this.cssClasses.innerItem}`),
            tl = new TimelineMax();
            if($.viewportW() < 767){
                item.each(function(index, item){
                    let trans = $(item).data('trans');
                    tl.to($(item), time, {alpha: 1, y: 0});
                }); 
                tl.to($('.second .img-two'), 0, {x: 0});
            } else {
                item.each(function(index, item){
                    let trans = $(item).data('trans');
                    tl.to($(item), time, {alpha: 1, y: trans});
                        
                });
                
            }
    }

    _menuFixedListener(){
        let st = $(window).scrollTop(),
            head = $(`.${this.cssClasses.header}`),
            
            stTop = 0,
            tl = new TimelineMax();
              
        if($.viewportW() > 767){
            stTop = 20
        } else {
            stTop = 10
        }
        if (st > this.lastScrollTop){
           
           if(st >= 80 && this.headerAnimation == true){
                tl.to(head, .4, {y: -70, onComplete: ()=>{
                    head.removeClass('fixed');
                    this.headerAnimation = false;
                    
            }});
           } else{
              
           }
           
        } else {
            
            
            if(this.headerAnimation == false){
                head.addClass('fixed');
                tl.to(head, .4, {y:0});
                this.headerAnimation = true;
            }

            if(st <= stTop){
                head.removeClass('fixed');
            }
            
        }
        this.lastScrollTop = st;

        if(st == 0){
            tl.to(head, 0, {y:0});
        }
    }

    _navScrollListener(){
        let st = $(window).scrollTop(),
            wH = $($.viewportH),
            imgBlock = $('.inner-row.first .text img'),
            nav = $('.inner-nav'),
            navOff = nav.find('.inner-nav-wrapp').offset().top,
            imgLine = imgBlock.offset().top + imgBlock.height();

        // console.log(navOff);
        // console.log(imgLine);

        if($.viewportW() > 768){
           
            if(navOff < (imgLine)){
                console.log('action')
                let newH = 98 - (imgLine - navOff);
                nav.height(newH);
            } else{
                nav.height(98);
            }

        }


    }
    
    resize(){
        $(window).on('resize', ()=>{
            
            this._loadAnimation('0');
            this._setMidleH();
        });
    }

    ajaxClick(){
        let link = $(`.${this.cssClasses.ajax}`);


        link.on('click', (e)=>{
            e.preventDefault();
            let target = $(e.currentTarget),
                beforeWrapp = $('.orrigin'),
                beforeWrappW = $('.ajax-wrapp').width(),
                src = target.attr('href'),
                nav = $('.inner-nav'),
                newAjax = $('.next-ajax'),
                tl = new TimelineMax();
            console.log(src);
            if(!target.hasClass('active')){
                link.removeClass('active');
                target.addClass('active');
                link.css('pointer-events', 'none');
                $.ajax({url: src, success: (result)=>{
                    // $(result).appendTo('.ajax-wrapp');
                    $('.ajax-wrapp').append(result);
                    this._loadAnimation('0');
                    this._setMidleH();
                    $('.next-ajax').imagesLoaded().done(( instance ) => {
                        console.log('all images successfully loaded');
                        tl
                            .to(nav, 0.4, {alpha: 0})
                            .to(beforeWrapp, 0.8, {x: -beforeWrappW})
                            .to($('.next-ajax'), 0.8, {x: 0, onComplete:()=>{
                                $('.orrigin').remove();
                                $('.inner-page_wrapp').removeClass('next-ajax').addClass('orrigin');
                                link.css('pointer-events', 'auto');
                            }}, "-=0.3")
                            .to(nav, 0.4, {alpha: 1, zIndex: 1})
                    });
                    
                        // .to($('.ajax-wrapp'), 0.6, {alpha: 1});
                    // this._loadAnimation();
                }});
            } else {
                return;
            }
        });
    }

    _init(){
        super._init();
        console.log('entertain slider init');
        setTimeout((e)=>{
            this._loadAnimation(0.4);
        }, 500);
        this._setMidleH();
        $(window).on('scroll', (e)=>{
            this._menuFixedListener();
            this._navScrollListener();
           
        });
        this._navScrollListener();
        this.ajaxClick();
        
        this.resize();
    }
}