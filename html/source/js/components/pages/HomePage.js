/**
 * Created by Eugene Khrapenko on 09.04.17.
 * email: wowhoman@gmail.com || ekh@inspired-interactive.com
 */

import AbstractPage from './AbstractPage';
import './../../plugins/swiper.min';
import './../../plugins/js.cookie';

window.tw = TweenMax;


export default class HomePage extends AbstractPage{
    constructor(){
        super();

        this.cssClasses = $.extend(true, {}, this.cssClasses, {
            'navLink': 'js-nav-link',
            'socialsLink': 'js-socials',
            'preloadWrapper': 'preloader',
            'preloadPattern': 'preload-pattern',
            'preloadLogo': 'preload-logo',
            'preloadText': 'preload-text',
            'mainSlider': 'js-home-slider',
            'sliderBtn': 'js-slider-btns',
            'menu': 'js-second-menu',
            'mobileBlock': 'js-mobile-home'
        });
        
        this.mobileSet = null;
        if($.viewportW() > 767){
            this.desktop = true;
           
        } else{
            this.desktop = false;
           
        }

        this._init();    
    }



    
    
    preload(type){
        let wrapp = $(`.${this.cssClasses.preloadWrapper}`),
            pattern = $(`.${this.cssClasses.preloadPattern}`),
            logo = $(`.${this.cssClasses.preloadLogo}`),
            text = $(`.${this.cssClasses.preloadText}`),
            navLink = $(`.${this.cssClasses.navLink}`),
            socials = $(`.${this.cssClasses.socialsLink}`),
            transitionText = ($(window).height() / 2 - ($(`.${this.cssClasses.preloadText}`).height() / 2)),
            transitionLogo = 0,
            tl = new TimelineMax();
        console.log(new Date, 2);
        if($.viewportW() < 767){
            transitionLogo = ($(window).height() / 2 ) - ($(`.${this.cssClasses.preloadLogo}`).height() / 2) - 40;
        } else {
            transitionLogo = ($(window).height() / 2 ) - ($(`.${this.cssClasses.preloadLogo}`).height() / 2) - 71;
        };

        if(type == 'long'){
             if($('.load-preload').length){
                $('body').removeClass('load-preload');
                setTimeout((e)=>{
                    text.css('transition', 'none').css('scale', 'auto');
                    tl
                        .to(pattern, 1, {alpha: 0})  
                        .to(logo, 1, {y: -transitionLogo - 15}, "-=1")
                        .to(text, 1, {y:  -transitionText, alpha: 0}, "-=1")
                        .to(wrapp, .4, {css: {alpha: 0}, onComplete: ()=>{
                            wrapp.remove();
                        }})
                        .to(navLink, .6, {alpha: 1, y: 0});
                        // .to(socials, .6, {alpha: 1, y: 0}, "-=0.6");
                }, 8500);
            } else {
                $('body').on('preload-animation-end', ()=>{
                    text.css('transition', 'none').css('scale', 'auto');
                    
                    tl
                        .to(pattern, 1, {alpha: 0}, "+=1.5")  
                        .to(logo, 1, {y: -transitionLogo}, "-=1")
                        .to(text, 1, {y:  -transitionText, alpha: 0}, "-=1")
                        .to(wrapp, .4, {css: {alpha: 0}, onComplete: ()=>{
                            wrapp.remove();
                        }})
                        .to(navLink, .6, {alpha: 1, y: 0});
                        // .to(socials, .6, {alpha: 1, y: 0}, "-=0.6");
                });
            }
        } else if(type == 'short') {

            console.log('short');
           
            tl
                 
                .to($('.short-preload'), 0.6,{alpha: 0}, "+=2")
                .to($('.white-pattern'), .6, {alpha: 0})
                .to(navLink, .6, {alpha: 1, y: 0, onComplete: ()=>{
                    $('.white-pattern').remove();
                }}, "+=0.5");
            $('.page-logo').find('path').css('transition', 'transform 0.4s ease 0s');    
        }
        // $('body').removeClass('loading');
       
    }    

    
    
    mainSliderInit(timeout){
        let sliderWrapp = $(`.${this.cssClasses.mainSlider}`),
            sliderBtn = $(`.${this.cssClasses.sliderBtn}`),
            sliderIndex = $('.swiper-slide-active').index(),
            btnIndex = sliderBtn.find('.active').index(),
            mobileBlock = $(`.${this.cssClasses.mobileBlock}`),
            currentActive = 0;
            
            console.log('reinit');
           if($.viewportW() > 767){
                let slider = new Swiper('.js-home-slider', {
                effect: 'fade',
                loop: 'true',
                speed: 1200,
                autoplay: 2000,
                autoplayDisableOnInteraction: false,
                onlyExternal: true,
                spaceBetween: 30,
                onSlideChangeStart: function (swiper) {
                    let curentLink = 0;
                    sliderBtn.find('a').removeClass('active');
                    if(swiper.activeIndex === 1 || swiper.activeIndex === 4){
                        curentLink = sliderBtn.find('a').eq(0);
                    } else {
                        curentLink = sliderBtn.find('a').eq(swiper.activeIndex -1);
                    }
                    curentLink.addClass('active');
                    //before Event use it for your purpose
                },
                onInit: function(swiper){
                    swiper.stopAutoplay();
                    setTimeout((e)=>{
                        swiper.unlockSwipes();
                        swiper.startAutoplay();
                        
                    }, timeout);
                    
                    sliderBtn.find('a').on('mouseenter', (e)=>{
                        
                        let target = $(e.currentTarget);
                        if(target.hasClass('active')){
                            swiper.stopAutoplay();
                        } else{
                             currentActive = swiper.activeIndex;
                             sliderBtn.find('a').css('overflow', 'hidden');
                             target.css('overflow', 'visible');
                            swiper.stopAutoplay();
                            let newSlide = target.index();
                            swiper.slideTo((newSlide + 1), 1000, false);
                        }
                        
                        

                    }); 

                    sliderBtn.find('a').on('mouseleave', (e)=>{
                        let target = $(e.currentTarget);

                        if(target.hasClass('active')){
                            swiper.startAutoplay();
                        } else{
                            swiper.slideTo((currentActive), 1000, false);
                            sliderBtn.find('a').css('overflow', 'visible');
                            swiper.startAutoplay();
                        }
                        
                        

                    }); 

                }
            });
           } else{
               this.mobileSet = setInterval((e)=>{
                    let currentBlock = mobileBlock.closest('.home-mobile').find('.active'),
                    currentBlockCss = mobileBlock.closest('.home-mobile').find('.js-mobile-home'),
                        firstSlide = mobileBlock.eq(0);
                    setTimeout(()=>{
                        currentBlockCss.css('transform','translate3d(0,0,0)');
                    },100)
                    if(currentBlock.index() === 2){
                        currentBlock.removeClass('active');
                        firstSlide.addClass('active');
                    } else{
                        currentBlock.removeClass('active');
                        currentBlock.next().addClass('active');
                    }
                    // console.log(currentBlock.index());
                    
                }, 2000);
           }
    }

    mobileMainHeight(){
        if($.viewportW() < 767){
            const imgHeight = $('.home-mobile_i.active').height();
            $('.mobile-img-main').height((imgHeight + 20));   
        }
    }

    resize(){
        $(window).on('resize', ()=>{
            console.log('home-resize');
            if($.viewportW() > 767 && this.desktop == false){
                this.desktop = true;
                
                clearInterval(this.mobileSet);
                $(window).trigger('breakpoint.switch'); 
            } else if( $.viewportW() < 768 && this.desktop == true){
                this.desktop = false;
                
                this.mobileMainHeight();
                // $('.js-home-slider').data('swiper').destroy();
                $(window).trigger('breakpoint.switch');
            }
        });
        $(window).on('breakpoint.switch', ()=>{
            console.log('switch');
            setTimeout(()=>{
                this.mainSliderInit(0); 
            }, 500);
            
        });
    }

    _init(){
        super._init();
        console.log('Home Page Init');
        this.mobileMainHeight();
        // $.removeCookie('visit', { path: '/' });
        if(!$.cookie.get('visit')){
            $('.white-pattern').remove();
            $('body').removeClass('loading');
            $('body').addClass('load-preload');
             $.cookie.set('visit', 'one', {path: '/' })
             this.preload('long');
             this.mainSliderInit(8500);
        } else {
            $('.preloader').remove();
            $('body').removeClass('loading');
            console.log(new Date, 1);
            $('.short-preload').removeAttr('style');
            setTimeout(()=>{
                $('body').addClass('loading-short');
                this.preload('short');
                this.mainSliderInit(2000);
            }, 100);
            // $('body').addClass('loading-short');
            // $('.short-preload').css('opacity', '1');
            // this.preload('short');
            // this.mainSliderInit(0);
        }

        
    }
}