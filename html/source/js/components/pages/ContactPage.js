import AbstractPage from './AbstractPage';
import './../../plugins/swiper.min';

export default class ContactPage extends AbstractPage{
    constructor(){
        super();

        this.cssClasses = $.extend(true, {}, this.cssClasses, {
            'contactItem': 'js-contact-animation',
            'contactSvg': 'js-contact-svg',
            'mailBtn': 'js-sent'
        });

        this._init();
    }

    _loadAnimation(){
        let contactItem = $(`.${this.cssClasses.contactItem}`),
            contactSvg = $(`.${this.cssClasses.contactSvg}`),
            path = $(`.${this.cssClasses.contactSvg}`).find('path'),
            tl = new TimelineMax();

        tl
            .to(contactItem, 1, {alpha: 0.8, y:0})
            .fromTo(contactSvg, 1, {alpha: 0, y: 200}, {alpha: 1, y: 0}, "-=0.5")
            .set(path, {alpha: 1}, "-=2");
            
    }

    _sentMail(){
        let sentBtn = $(`.${this.cssClasses.mailBtn}`),
            contactItem = $(`.${this.cssClasses.contactItem}`);

        sentBtn.on('click', (e)=>{
            let inputMail = $('.input input'),
                mailData = $('#inp').val(),
                testEmail = /^([a-z0-9_\.-])+@[a-z0-9-]+\.([a-z]{2,4}\.)?[a-z]{2,4}$/i,
                mail = contactItem.find('p'),
                title = contactItem.find('.title'),
                inputWrapp = contactItem.find('.input'),
                thenks = contactItem.find('.contact-thenks-messege'),
                tl = new TimelineMax();

            if(!inputWrapp.hasClass('send')){

                if(mailData != '') {

                    if(testEmail.test(mailData)){
                        console.log('sucsess');
                        inputWrapp.addClass('send');
                        if(inputMail.hasClass('error')){
                            inputMail.removeClass('error');
                        }
                        tl
                            .to(mail, .4, {alpha: 0})
                            .to(title, .4, {alpha: 0}, "-=0.4")
                            .to(inputWrapp, .4, {alpha: 0}, "-=0.4")
                            .to(thenks, .4, {alpha: 1});
                        $.ajax({
                            url: '/test/action.php',
                            type: 'post',
                            dataType: 'json',
                            data: {
                                email: mailData
                            },
                            success: function(response) {
                                if (response.success) {
                            
                                } else {
                                
                                }
                            },
                            error: function() {
                            }
                        });
                    } else {
                        console.log('fuck')
                        inputMail.addClass('error');
                    }
                } else {
                    inputMail.addClass('error');
                }


            } else{
                tl
                    .to(thenks, .4,{alpha: 0})
                    .to(mail, .4, {alpha: 1})
                    .to(title, .4, {alpha: 1}, "-=0.4")
                    .to(inputWrapp, .4, {alpha: 1}, "-=0.4");
            }
            
        });

    }
    

    _init(){
        super._init();
        console.log('contact init');

        setTimeout((e)=>{
            this._loadAnimation();
            this._sentMail();
        }, 1000);
    }
}